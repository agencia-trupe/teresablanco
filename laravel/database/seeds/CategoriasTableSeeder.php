<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriasTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('categorias')->insert([
            'id'        => 1,
            'ordem'     => 1,
            'titulo_pt' => 'Poltronas',
            'titulo_en' => 'Armchairs',
            'titulo_es' => 'Sillones',
            'slug'      => 'poltronas',
            'frase_pt'  => '',
            'frase_en'  => '',
            'frase_es'  => '',
            'capa'      => ''
        ]);

        DB::table('categorias')->insert([
            'id'        => 2,
            'ordem'     => 2,
            'titulo_pt' => 'Banquetas',
            'titulo_en' => 'Stools',
            'titulo_es' => 'Banquetas',
            'slug'      => 'banquetas',
            'frase_pt'  => '',
            'frase_en'  => '',
            'frase_es'  => '',
            'capa'      => ''
        ]);

        DB::table('categorias')->insert([
            'id'        => 3,
            'ordem'     => 3,
            'titulo_pt' => 'Decoração',
            'titulo_en' => 'Decoration',
            'titulo_es' => 'Decoración',
            'slug'      => 'decoracao',
            'frase_pt'  => '',
            'frase_en'  => '',
            'frase_es'  => '',
            'capa'      => ''
        ]);
    }
}
