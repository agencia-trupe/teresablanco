<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
        $this->call(ContatosTableSeeder::class);
        $this->call(HomeTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
    }
}
