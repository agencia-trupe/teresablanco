<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'imagem_artista'    => '',
            'frase_artista_pt'  => 'Com olhar apurado e alma de artista, Teresa Blanco desenvolve peças exclusivas para criar novas atmosferas.',
            'frase_artista_en'  => "With a keen eye and an artist's soul, Teresa Blanco develops exclusive pieces to create new atmospheres.",
            'frase_artista_es'  => 'Con buen ojo y alma de artista, Teresa Blanco desarrolla piezas exclusivas para crear nuevos ambientes.',
            'texto_artista_pt'  => '',
            'texto_artista_en'  => '',
            'texto_artista_es'  => '',
            'imagem_design'  => '',
            'frase_design_pt'   => 'Poltronas, bancos e objetos de decoração',
            'frase_design_en'   => 'Armchairs, benches and decorative objects',
            'frase_design_es'   => 'Sillones, bancos y objetos de decoración',
            'texto_design_pt'   => '',
            'texto_design_en'   => '',
            'texto_design_es'   => '',
        ]);
    }
}
