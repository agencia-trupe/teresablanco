<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos')->insert([
            'nome'          => 'Teresa Blanco',
            'email'         => 'contato@trupe.net',
            'telefone'      => '+55 11 4306-6393',
            'celular'       => '+55 11 97499-6393',
            'whatsapp'      => '+55 11 97499-6393',
            'instagram'     => 'https://www.instagram.com/teresablancodesign/',
            'facebook'      => 'https://www.facebook.com/teresablancodesign/',
            'endereco_pt'   => 'Rua Fidêncio Ramos, 160 - conjs. 1613/1614',
            'endereco_en'   => 'Rua Fidêncio Ramos, 160 - set 1613/1614',
            'endereco_es'   => 'Rua Fidêncio Ramos, 160 - set 1613/1614',
            'bairro_pt'     => 'Vila Olímpia',
            'bairro_en'     => 'Vila Olímpia',
            'bairro_es'     => 'Vila Olímpia',
            'cidade_pt'     => 'São Paulo - SP',
            'cidade_en'     => 'São Paulo - SP',
            'cidade_es'     => 'São Paulo - SP',
            'cep'           => '04551-010',
            'link_mapa'     => 'https://goo.gl/maps/vBDci7NKk5HRENQH9',
        ]);
    }
}
