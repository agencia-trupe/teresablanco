<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem_artista');
            $table->string('frase_artista_pt');
            $table->string('frase_artista_en');
            $table->string('frase_artista_es');
            $table->text('texto_artista_pt');
            $table->text('texto_artista_en');
            $table->text('texto_artista_es');
            $table->string('imagem_design');
            $table->string('frase_design_pt');
            $table->string('frase_design_en');
            $table->string('frase_design_es');
            $table->text('texto_design_pt');
            $table->text('texto_design_en');
            $table->text('texto_design_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
