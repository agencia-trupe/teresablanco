<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->text('descricao_pt');
            $table->text('descricao_en');
            $table->text('descricao_es');
            $table->string('peso_pt')->nullable();
            $table->string('peso_en')->nullable();
            $table->string('peso_es')->nullable();
            $table->string('altura_pt')->nullable();
            $table->string('altura_en')->nullable();
            $table->string('altura_es')->nullable();
            $table->string('largura_pt')->nullable();
            $table->string('largura_en')->nullable();
            $table->string('largura_es')->nullable();
            $table->string('profundidade_pt')->nullable();
            $table->string('profundidade_en')->nullable();
            $table->string('profundidade_es')->nullable();
            $table->string('diametro_pt')->nullable();
            $table->string('diametro_en')->nullable();
            $table->string('diametro_es')->nullable();
            $table->string('desenho')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos');
    }
}
