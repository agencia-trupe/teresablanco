<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('celular');
            $table->string('whatsapp');
            $table->string('instagram');
            $table->string('facebook');
            $table->string('endereco_pt');
            $table->string('endereco_en');
            $table->string('endereco_es');
            $table->string('bairro_pt');
            $table->string('bairro_en');
            $table->string('bairro_es');
            $table->string('cidade_pt');
            $table->string('cidade_en');
            $table->string('cidade_es');
            $table->string('cep');
            $table->string('link_mapa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contatos');
    }
}
