<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateContatosRecebidosTable extends Migration
{
    public function up()
    {
        Schema::create('contatos_recebidos', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('arquiteto_design')->default(0);
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->text('mensagem');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contatos_recebidos');
    }
}
