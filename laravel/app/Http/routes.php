<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('produtos/{categoria}', 'ProdutosController@index')->name('produtos.categorias');
    Route::get('produtos/{categoria}/{produto}', 'ProdutosController@show')->name('produtos.detalhes');
    Route::post('mais-informacoes/{produto}', 'ContatoController@postMaisInformacoes')->name('mais-informacoes');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Idiomas
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt', 'en', 'es'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->back();
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('contatos/recebidos/{recebidos}/toggle', ['as' => 'painel.contatos.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::get('contatos/mais-informacoes/{informacoes}/toggle', ['as' => 'painel.contatos.mais-informacoes.toggle', 'uses' => 'ContatosInformacoesController@toggle']);

        Route::resource('usuarios', 'UsuariosController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');
        Route::resource('contatos/recebidos', 'ContatosRecebidosController');
        Route::resource('contatos/mais-informacoes', 'ContatosInformacoesController');
        Route::resource('contatos', 'ContatosController');
        Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
        Route::resource('banners', 'BannersController');
        Route::resource('categorias', 'CategoriasController', ['only' => ['index', 'edit', 'update']]);
        Route::resource('produtos', 'ProdutosController');
        Route::get('produtos/{produto}/imagens/clear', [
            'as'   => 'painel.produtos.imagens.clear',
            'uses' => 'ProdutosImagensController@clear'
        ]);
        Route::resource('produtos.imagens', 'ProdutosImagensController', ['parameters' => ['imagens' => 'produtos_imagens']]);
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('esqueci-minha-senha', 'PasswordController@sendEmail')->name('esqueci-minha-senha');
        Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')->name('esqueci-minha-senha.post');
        Route::get('reset/{token}', 'PasswordController@showResetForm')->name('password-reset');
        Route::post('reset', 'PasswordController@reset')->name('password-reset.post');
    });
});
