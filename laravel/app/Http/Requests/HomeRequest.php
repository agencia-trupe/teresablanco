<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem_artista'   => 'image',
            'frase_artista_pt' => 'required',
            'frase_artista_en' => 'required',
            'frase_artista_es' => 'required',
            'texto_artista_pt' => 'required',
            'texto_artista_en' => 'required',
            'texto_artista_es' => 'required',
            'imagem_design'    => 'image',
            'frase_design_pt'  => 'required',
            'frase_design_en'  => 'required',
            'frase_design_es'  => 'required',
            'texto_design_pt'  => 'required',
            'texto_design_en'  => 'required',
            'texto_design_es'  => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_artista'] = 'image';
            $rules['imagem_design'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
