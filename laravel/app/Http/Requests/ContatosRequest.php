<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'          => 'required',
            'email'         => 'required|email',
            'telefone'      => 'required',
            'celular'       => 'required',
            'whatsapp'      => 'required',
            'instagram'     => 'required',
            'facebook'      => 'required',
            'endereco_pt'   => 'required',
            'endereco_en'   => 'required',
            'endereco_es'   => 'required',
            'bairro_pt'     => 'required',
            'bairro_en'     => 'required',
            'bairro_es'     => 'required',
            'cidade_pt'     => 'required',
            'cidade_en'     => 'required',
            'cidade_es'     => 'required',
            'link_mapa'     => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
            'email'    => "Insira um endereço de e-mail válido.",
        ];
    }
}
