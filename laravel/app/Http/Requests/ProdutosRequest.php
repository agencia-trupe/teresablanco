<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt'    => 'required',
            'titulo_en'    => 'required',
            'titulo_es'    => 'required',
            'descricao_pt' => 'required',
            'descricao_en' => 'required',
            'descricao_es' => 'required',
            'desenho'      => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['desenho'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
