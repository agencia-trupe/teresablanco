<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CategoriasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'frase_pt'  => 'required',
            'frase_en'  => 'required',
            'frase_es'  => 'required',
            'capa'      => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
