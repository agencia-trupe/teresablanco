<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Produto;
use App\Models\ProdutoImagem;

class ProdutosController extends Controller
{
    public function index($categoria_slug)
    {
        $categoria = Categoria::where('slug', $categoria_slug)->first();

        $categorias = Categoria::ordenados()->get();

        $produtos = Produto::categoria($categoria->id)->ordenados()->get();
        $imagens = ProdutoImagem::ordenados()->get();

        return view('frontend.produtos-categorias', compact('categoria', 'categorias', 'produtos', 'imagens'));
    }

    public function show($categoria_slug, $produto_slug)
    {
        $categoria = Categoria::where('slug', $categoria_slug)->first();
        $categorias = Categoria::ordenados()->get();

        $produto = Produto::where('slug', $produto_slug)->where('categoria_id', $categoria->id)->first();
        $imagens = ProdutoImagem::produto($produto->id)->ordenados()->get();
        $capa = $imagens->first();

        $proximo = Produto::where('categoria_id', $categoria->id)->ordenados()->where('ordem', '>', $produto->ordem)->first();
        $anterior = Produto::where('categoria_id', $categoria->id)->where('ordem', '<', $produto->ordem)->orderBy('ordem', 'desc')->first();

        return view('frontend.produtos-detalhes', compact('produto', 'imagens', 'categoria', 'categorias', 'capa', 'proximo', 'anterior'));
    }
}
