<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Banner;
use App\Models\Categoria;
use App\Models\Home;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $home = Home::first();
        $banners = Banner::ordenados()->get();
        $categorias = Categoria::ordenados()->get();

        return view('frontend.home', compact('home', 'banners', 'categorias'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
