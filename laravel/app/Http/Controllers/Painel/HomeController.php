<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomeRequest;
use App\Models\Home;

class HomeController extends Controller
{
    public function index()
    {
        $registro = Home::first();

        return view('painel.home.edit', compact('registro'));
    }

    public function update(HomeRequest $request, Home $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_artista'])) $input['imagem_artista'] = Home::upload_imagem_artista();
            if (isset($input['imagem_design'])) $input['imagem_design'] = Home::upload_imagem_design();

            $registro->update($input);

            return redirect()->route('painel.home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
