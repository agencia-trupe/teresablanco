<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\ContatoInformacao;
use App\Models\Produto;

class ContatosInformacoesController extends Controller
{
    public function index()
    {
        $contatosinfos = ContatoInformacao::orderBy('created_at', 'DESC')->get();
        $produtos = Produto::all();

        return view('painel.contatos.informacoes.index', compact('contatosinfos', 'produtos'));
    }

    public function show($contatoInfo)
    {
        $contato = ContatoInformacao::where('id', $contatoInfo)->first();
        $contato->update(['lido' => 1]);

        $produto = Produto::where('id', $contato->produto_id)->first();

        return view('painel.contatos.informacoes.show', compact('contato', 'produto'));
    }

    public function destroy($contatoInfo)
    {
        try {
            $contato = ContatoInformacao::where('id', $contatoInfo)->first();
            $contato->delete();

            return redirect()->route('painel.contatos.mais-informacoes.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($contatoInfo)
    {
        try {
            $contato = ContatoInformacao::where('id', $contatoInfo)->first();
            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.contatos.mais-informacoes.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
