<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosRequest;
use App\Models\Categoria;
use App\Models\Produto;

class ProdutosController extends Controller
{
    public function index()
    {
        $categorias = Categoria::ordenados()->get();

        if (isset($_GET['categoria'])) {
            $categoria = $_GET['categoria'];
            $produtos = Produto::categoria($categoria)->ordenados()->get();
        } else {
            $produtos = Produto::ordenados()->get();
        }

        return view('painel.produtos.index', compact('produtos', 'categorias'));
    }

    public function create()
    {
        $categorias = Categoria::ordenados()->pluck('titulo_pt', 'id');

        return view('painel.produtos.create', compact('categorias'));
    }

    public function store(ProdutosRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo_pt, '-');

            if (isset($input['desenho'])) $input['desenho'] = Produto::upload_desenho();

            $count = count(Produto::where('slug', $input['slug'])->where('categoria_id', $input['categoria_id'])->get());

            if ($count == 0) {
                Produto::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse registro já existe']);
            }

            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Produto $produto)
    {
        $categorias = Categoria::ordenados()->pluck('titulo_pt', 'id');

        return view('painel.produtos.edit', compact('produto', 'categorias'));
    }

    public function update(ProdutosRequest $request, Produto $produto)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo_pt, '-');

            if (isset($input['desenho'])) $input['desenho'] = Produto::upload_desenho();

            $produto->update($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Produto $produto)
    {
        try {
            $produto->delete();

            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
