<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriasRequest;
use App\Models\Categoria;

class CategoriasController extends Controller
{
    public function index()
    {
        $categorias = Categoria::ordenados()->get();

        return view('painel.categorias.index', compact('categorias'));
    }

    public function edit(Categoria $categoria)
    {
        return view('painel.categorias.edit', compact('categoria'));
    }

    public function update(CategoriasRequest $request, Categoria $categoria)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo_pt, '-');

            if (isset($input['capa'])) $input['capa'] = Categoria::upload_capa();

            $categoria->update($input);

            return redirect()->route('painel.categorias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
