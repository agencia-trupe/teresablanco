<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosImagensRequest;
use App\Models\Produto;
use App\Models\ProdutoImagem;

class ProdutosImagensController extends Controller
{
    public function index(Produto $produto)
    {
        $imagens = ProdutoImagem::produto($produto->id)->ordenados()->get();

        return view('painel.produtos.imagens.index', compact('imagens', 'produto'));
    }

    public function show(Produto $produto, ProdutoImagem $imagem)
    {
        return $imagem;
    }

    public function store(Produto $produto, ProdutosImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = ProdutoImagem::uploadImagem();
            $input['produto_id'] = $produto->id;

            $imagem = ProdutoImagem::create($input);

            $view = view('painel.produtos.imagens.imagem', compact('produto', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Produto $produto, ProdutoImagem $imagem)
    {
        try {
            $imagem->delete();

            return redirect()->route('painel.produtos.imagens.index', $produto)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear(Produto $produto)
    {
        try {
            $produto->imagens()->delete();

            return redirect()->route('painel.produtos.imagens.index', $produto)
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
