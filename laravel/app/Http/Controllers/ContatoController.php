<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContatosInformacoesRequest;
use App\Http\Requests\ContatosRecebidosRequest;
use App\Models\Contato;
use App\Models\ContatoInformacao;
use App\Models\ContatoRecebido;
use App\Models\Produto;
use Illuminate\Support\Facades\Mail;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $data = $request->all();
        
        if($request->arquiteto_design) {
            $data['arquiteto_design'] = $request->arquiteto_design;
        } else {
            $data['arquiteto_design'] = 0;
        }

        $contatoRecebido->create($data);
        $this->sendMail($data);

        return redirect('contato')->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('emails.contato', $data, function ($m) use ($email, $data) {
            $m->to($email, config('app.name'))
                ->subject('[CONTATO] ' . config('app.name'))
                ->replyTo($data['email'], $data['nome']);
        });
    }

    public function postMaisInformacoes(ContatosInformacoesRequest $request, $produto_id)
    {
        $data = $request->all();
        $data['produto_id'] = $produto_id;
        
        ContatoInformacao::create($data);
        $this->sendMailInformacoes($data);

        return redirect()->back()->with('enviado', true);
    }

    private function sendMailInformacoes($data)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }
        $produto = Produto::where('id', $data['produto_id'])->first();

        Mail::send('emails.informacoes', ['data' => $data, 'produto' => $produto], function ($m) use ($email, $data) {
            $m->to($email, config('app.name'))
                ->subject('[CONTATO] ' . config('app.name'))
                ->replyTo($data['email'], $data['nome']);
        });
    }
}
