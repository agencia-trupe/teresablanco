<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('aceite_de_cookies', 'App\Models\AceiteDeCookies');
        $router->model('contatos_informacoes', 'App\Models\ContatoInformacao');
        $router->model('produtos_imagens', 'App\Models\ProdutoImagem');
        $router->model('produtos', 'App\Models\Produto');
        $router->model('categorias', 'App\Models\Categoria');
        $router->model('banners', 'App\Models\Banner');
        $router->model('home', 'App\Models\Home');
        $router->model('contatos_recebidos', 'App\Models\ContatoRecebido');
        $router->model('contatos', 'App\Models\Contato');
        $router->model('politica_de_privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('configuracoes', 'App\Models\Configuracao');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
