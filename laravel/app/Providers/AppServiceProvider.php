<?php

namespace App\Providers;

use App\Models\AceiteDeCookies;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('config', \App\Models\Configuracao::first());
        });

        view()->composer('frontend.*', function ($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('categoria1', \App\Models\Categoria::ordenados()->first());
            
            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());
        });

        view()->composer('painel.common.*', function ($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('contatosInfosNaoLidos', \App\Models\ContatoInformacao::naoLidos()->count());
        });
    }

    public function register()
    {
        //
    }
}
