<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $id)
    {
        return $query->where('categoria_id', $id);
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProdutoImagem', 'produto_id')->ordenados();
    }

    public static function upload_desenho()
    {
        return CropImage::make('desenho', [
            [
                'width'  => 400,
                'height' => null,
                'path'   => 'assets/img/produtos/'
            ],
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/produtos/originais/'
            ]
        ]);
    }
}
