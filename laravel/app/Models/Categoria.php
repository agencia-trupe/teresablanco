<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 520,
                'height' => null,
                'path'   => 'assets/img/categorias/'
            ],
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/categorias/originais/'
            ]
        ]);
    }
}
