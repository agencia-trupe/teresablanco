<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ProdutoImagem extends Model
{
    protected $table = 'produtos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 100,
                'height'  => 100,
                'path'    => 'assets/img/produtos/imagens/thumbs/'
            ],
            [
                'width'  => 350,
                'height' => 350,
                'upsize'  => true,
                'path'    => 'assets/img/produtos/imagens/'
            ],
            [
                'width'  => null,
                'height' => null,
                'path'    => 'assets/img/produtos/imagens/originais/'
            ]
        ]);
    }
}
