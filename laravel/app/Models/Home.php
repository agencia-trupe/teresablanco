<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_imagem_artista()
    {
        return CropImage::make('imagem_artista', [
            [
                'width'  => 300,
                'height' => null,
                'path'   => 'assets/img/home/'
            ],
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/home/originais/'
            ]
        ]);
    }

    public static function upload_imagem_design()
    {
        return CropImage::make('imagem_design', [
            [
                'width'  => 1980,
                'height' => null,
                'path'   => 'assets/img/home/'
            ],
            [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/home/originais/'
            ]
        ]);
    }
}
