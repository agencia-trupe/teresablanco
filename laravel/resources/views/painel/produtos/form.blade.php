@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('categoria_id', 'Categoria') !!}
    {!! Form::select('categoria_id', $categorias , old('categoria_id'), ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título [PT]') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [EN]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título [ES]') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao_pt', 'Descrição [PT]') !!}
            {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao_en', 'Descrição [EN]') !!}
            {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('descricao_es', 'Descrição [ES]') !!}
            {!! Form::textarea('descricao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4 form-group">
        {!! Form::label('peso_pt', 'Peso [PT]') !!}
        {!! Form::text('peso_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('peso_en', 'Peso [EN]') !!}
        {!! Form::text('peso_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('peso_es', 'Peso [ES]') !!}
        {!! Form::text('peso_es', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-4 form-group">
        {!! Form::label('diametro_pt', 'Diâmetro [PT]') !!}
        {!! Form::text('diametro_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('diametro_en', 'Diâmetro [EN]') !!}
        {!! Form::text('diametro_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('diametro_es', 'Diâmetro [ES]') !!}
        {!! Form::text('diametro_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-4 form-group">
        {!! Form::label('altura_pt', 'Altura [PT]') !!}
        {!! Form::text('altura_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('altura_en', 'Altura [EN]') !!}
        {!! Form::text('altura_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('altura_es', 'Altura [ES]') !!}
        {!! Form::text('altura_es', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-4 form-group">
        {!! Form::label('largura_pt', 'Largura [PT]') !!}
        {!! Form::text('largura_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('largura_en', 'Largura [EN]') !!}
        {!! Form::text('largura_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('largura_es', 'Largura [ES]') !!}
        {!! Form::text('largura_es', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-4 form-group">
        {!! Form::label('profundidade_pt', 'Profundidade [PT]') !!}
        {!! Form::text('profundidade_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('profundidade_en', 'Profundidade [EN]') !!}
        {!! Form::text('profundidade_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('profundidade_es', 'Profundidade [ES]') !!}
        {!! Form::text('profundidade_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="well form-group">
    {!! Form::label('desenho', 'Desenho') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$produto->desenho) }}" style="display:block; margin-bottom: 10px; max-width: 100%; max-height:200px">
    @endif
    {!! Form::file('desenho', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>