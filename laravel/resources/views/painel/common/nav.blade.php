<ul class="nav navbar-nav">

    <li class="dropdown @if(Tools::routeIs(['painel.banners*', 'painel.home*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.home.index')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Dados</a>
            </li>
            <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.categorias*')) class="active" @endif>
        <a href="{{ route('painel.categorias.index') }}">Categorias</a>
    </li>

    <li @if(Tools::routeIs('painel.produtos*')) class="active" @endif>
        <a href="{{ route('painel.produtos.index') }}">Produtos</a>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.contatos*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contatos
            @if($contatosNaoLidos >= 1 || $contatosInfosNaoLidos >= 1)
            @php $total = $contatosNaoLidos + $contatosInfosNaoLidos; @endphp
            <span class="label label-success" style="margin-left:3px;">{{ $total }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contatos.index')) class="active" @endif>
                <a href="{{ route('painel.contatos.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contatos.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contatos.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li @if(Tools::routeIs('painel.contatos.mais-informacoes*')) class="active" @endif>
                <a href="{{ route('painel.contatos.mais-informacoes.index') }}">
                    Mais Informações
                    @if($contatosInfosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosInfosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>

</ul>