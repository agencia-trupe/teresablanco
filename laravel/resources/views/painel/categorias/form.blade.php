@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título [PT]') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [EN]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título [ES]') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_pt', 'Frase [PT]') !!}
            {!! Form::text('frase_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_en', 'Frase [EN]') !!}
            {!! Form::text('frase_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_es', 'Frase [ES]') !!}
            {!! Form::text('frase_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/categorias/'.$categoria->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.categorias.index') }}" class="btn btn-default btn-voltar">Voltar</a>