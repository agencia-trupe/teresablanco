@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Categorias
    </h2>
</legend>


@if(!count($categorias))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="categorias">
    <thead>
        <tr>
            <th>Capa</th>
            <th>Título</th>
            <th>Frase</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($categorias as $categoria)
        <tr class="tr-row" id="{{ $categoria->id }}">
            <td><img src="{{ asset('assets/img/categorias/'.$categoria->capa) }}" style="width: 100%; max-width:150px;" alt=""></td>
            <td>{{ $categoria->titulo_pt }}</td>
            <td>{{ $categoria->frase_pt }}</td>
            <td class="crud-actions">
                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.categorias.edit', $categoria->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection