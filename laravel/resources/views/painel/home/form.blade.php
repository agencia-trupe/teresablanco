@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem_artista', 'Artista - Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/home/'.$registro->imagem_artista) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_artista', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_artista_pt', 'Artista - Frase [PT]') !!}
            {!! Form::text('frase_artista_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_artista_en', 'Artista - Frase [EN]') !!}
            {!! Form::text('frase_artista_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_artista_es', 'Artista - Frase [ES]') !!}
            {!! Form::text('frase_artista_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_artista_pt', 'Artista - Texto [PT]') !!}
            {!! Form::textarea('texto_artista_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_artista_en', 'Artista - Texto [EN]') !!}
            {!! Form::textarea('texto_artista_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_artista_es', 'Artista - Texto [ES]') !!}
            {!! Form::textarea('texto_artista_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem_design', 'Design - Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/home/'.$registro->imagem_design) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_design', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_design_pt', 'Design - Frase [PT]') !!}
            {!! Form::text('frase_design_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_design_en', 'Design - Frase [EN]') !!}
            {!! Form::text('frase_design_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_design_es', 'Design - Frase [ES]') !!}
            {!! Form::text('frase_design_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_design_pt', 'Design - Texto [PT]') !!}
            {!! Form::textarea('texto_design_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_design_en', 'Design - Texto [EN]') !!}
            {!! Form::textarea('texto_design_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_design_es', 'Design - Texto [ES]') !!}
            {!! Form::textarea('texto_design_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}