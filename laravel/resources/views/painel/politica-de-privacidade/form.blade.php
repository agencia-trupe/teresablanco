@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_pt', 'Texto - Política de Privacidade [PT]') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto - Política de Privacidade [EN]') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_es', 'Texto - Política de Privacidade [ES]') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}