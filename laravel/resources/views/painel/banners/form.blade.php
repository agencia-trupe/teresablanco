@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$banner->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_pt', 'Frase Destaque [PT]') !!}
            {!! Form::textarea('frase_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_en', 'Frase Destaque [EN]') !!}
            {!! Form::textarea('frase_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('frase_es', 'Frase Destaque [ES]') !!}
            {!! Form::textarea('frase_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('link', 'Link (opcional)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>