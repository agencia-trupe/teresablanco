<!DOCTYPE html>
<html>

<head>
    <title>[MAIS INFORMAÇÕES] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $data['nome'] }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $data['email'] }}</span><br>
    @if($data['telefone'])
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $data['telefone'] }}</span><br>
    @endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $data['mensagem'] }}</span>

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Produto:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $produto->titulo }}</span><br>

</body>

</html>