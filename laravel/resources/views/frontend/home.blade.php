@extends('frontend.common.template')

@section('content')

<section class="home">

    <div class="topo-banners">
        <div class="left">
            <div class="titulo-banner"></div>
            <div class="icones">
                <a href="{{ route('produtos.categorias', $categorias[0]->slug) }}" class="poltronas">
                    <div class="img-icone"></div>
                    <p class="titulo">{{ $categorias[0]->{trans('database.titulo')} }}</p>
                </a>
                <a href="{{ route('produtos.categorias', $categorias[1]->slug) }}" class="bancos">
                    <div class="img-icone"></div>
                    <p class="titulo">{{ $categorias[1]->{trans('database.titulo')} }}</p>
                </a>
                <a href="{{ route('produtos.categorias', $categorias[2]->slug) }}" class="decoracao">
                    <div class="img-icone"></div>
                    <p class="titulo">{{ $categorias[2]->{trans('database.titulo')} }}</p>
                </a>
            </div>
        </div>
        <div class="banners">
            @foreach($banners as $banner)
            @if($banner->link)
            <a href="{{ $banner->link }}" class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})">
                <h1 class="frase-banner">{!! $banner->{trans('database.frase')} !!}</h1>
            </a>
            @else
            <div class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})">
                <h1 class="frase-banner">{!! $banner->{trans('database.frase')} !!}</h1>
            </div>
            @endif
            @endforeach
            <img src="{{ asset('assets/img/layout/faixa-colorida.svg') }}" alt="" class="img-faixa-colorida">
            <div class=cycle-pager></div>
        </div>
    </div>

    <div class="artista" id="homeArtista">
        <div class="apresentacao">
            <div class="dados parallax-window" data-parallax="scroll">
                <h4 class="titulo parallax_scroll">{{ trans('frontend.home.artista') }}</h4>
                <h2 class="nome parallax_scroll">{{ $contato->nome }}</h2>
                <p class="frase parallax_scroll">{{ $home->{trans('database.frase_artista')} }}</p>
            </div>
            <div class="capa-artista">
                <img src="{{ asset('assets/img/home/'.$home->imagem_artista) }}" alt="" class="img-artista">
            </div>
        </div>
        <div class="sobre">
            <div class="texto-artista">{!! $home->{trans('database.texto_artista')} !!}</div>
            <a href="{{ route('produtos.categorias', $categorias[0]->slug) }}" class="btn-conheca">{{ trans('frontend.home.conheca') }}</a>
        </div>
    </div>

    <div class="design" id="homeDesign" style="background-image: url({{ asset('assets/img/home/'.$home->imagem_design) }})">
        <div class="centralizado">
            <div class="dados-design">
                <h4 class="titulo">{{ trans('frontend.home.design') }}</h4>
                <p class="subtitulo">{{ $home->{trans('database.frase_design')} }}</p>
                <div class="texto-design">{!! $home->{trans('database.texto_design')} !!}</div>
                <a href="{{ $contato->instagram }}" class="instagram" target="_blank">
                    <img src="{{ asset('assets/img/layout/ico-instagram-vermelho.svg') }}" alt="" class="img-instagram">
                    <div class="texto-instagram">{{ trans('frontend.home.siga') }}</div>
                </a>
            </div>
        </div>
    </div>

</section>

@endsection