@extends('frontend.common.template')

@section('content')

<section class="politica-de-privacidade">
    <h2 class="titulo" style="text-transform: uppercase;">{{ trans('frontend.politica') }}</h2>
    <div class="texto">
        {!! $politica->{trans('database.texto')} !!}
    </div>
</section>

@endsection