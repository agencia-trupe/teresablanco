@extends('frontend.common.template')

@section('content')

<section class="contato">
    <div class="centralizado">
        <div class="dados-contato">
            <img src="{{ asset('assets/img/layout/img-contato.jpg') }}" alt="" class="img-contato">
            <div class="dados">
                <div class="redes-sociais">
                    <a href="{{ $contato->facebook }}" target="_blank" class="facebook">
                        <img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt="" class="img-rede">
                    </a>
                    <a href="{{ $contato->instagram }}" target="_blank" class="instagram">
                        <img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="" class="img-rede">
                    </a>
                    @php $whatsapp = str_replace("-", "", str_replace(" ", "", $contato->celular)); @endphp
                    <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="whatsapp" target="_blank">
                        <img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt="" class="img-rede">
                    </a>
                </div>
                <p class="showroom">SHOWROOM</p>
                <p class="endereco">{{ $contato->{trans('database.endereco')} }}</p>
                <p class="bairro-cidade">{{ $contato->{trans('database.bairro')} }} - {{ $contato->{trans('database.cidade')} }}</p>
                <p class="cep">{{ trans('frontend.footer.cep') }} {{ $contato->cep }}</p>
                <a href="{{ $contato->link_mapa }}" target="_blank" class="link-mapa">{{ trans('frontend.footer.mapa') }}</a>
                @php
                $telefone = str_replace("-", "", str_replace(" ", "", $contato->telefone));
                $celular = str_replace("-", "", str_replace(" ", "", $contato->celular));
                @endphp
                <a href="tel:{{ $telefone }}" class="telefone">T. {{ $contato->telefone }}</a>
                <a href="https://api.whatsapp.com/send?phone={{ $celular }}" class="celular" target="_blank">{{ trans('frontend.footer.celular') }} {{ $contato->celular }}</a>
                <a href="mailto:{{ $contato->email }}" target="_blank" class="email">{{ $contato->email }}</a>
            </div>
        </div>
        <div class="form-contato">
            <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="checkbox">
                    <input type="checkbox" name="arquiteto_design" id="arquiteto_design" value="1">
                    <label for="arquiteto_design">{{ trans('frontend.contato.checkbox') }}</label>
                </div>
                <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="{{ trans('frontend.contato.telefone') }}" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-contato">{{ trans('frontend.contato.enviar') }}</button>

                @if($errors->any())
                <div class="flash flash-erro">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                    @if($errors->has('g-recaptcha-response'))
                    <br>{{ trans('frontend.contato.recaptcha') }}.
                    @endif
                </div>
                @endif

                @if(session('enviado'))
                <div class="flash flash-sucesso">
                    <p>{{ trans('frontend.contato.sucesso') }}!</p>
                </div>
                @endif
            </form>
        </div>
    </div>
</section>

@endsection