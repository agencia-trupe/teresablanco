<footer>

    <div class="redes-sociais">
        @php $whatsapp = str_replace("-", "", str_replace(" ", "", $contato->celular)); @endphp
        <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="whatsapp" target="_blank">
            <img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt="" class="img-rede">
            <p class="texto-whatsapp">{{ trans('frontend.footer.whatsapp') }}</p>
        </a>
        <div class="redes">
            <a href="{{ $contato->facebook }}" target="_blank" class="facebook">
                <img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt="" class="img-rede">
            </a>
            <a href="{{ $contato->instagram }}" target="_blank" class="instagram">
                <img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="" class="img-rede">
            </a>
        </div>
        <p class="texto-redes">{{ trans('frontend.footer.siga') }} {{ $contato->nome }}</p>
    </div>

    <div class="informacoes">
        <a href="{{ route('home') }}" class="logo-footer" title="{{ $config->title }}">
            <img src="{{ asset('assets/img/layout/marca-teresablanco.svg') }}" alt="" class="img-logo">
        </a>
        <div class="dados">
            <p class="showroom">SHOWROOM</p>
            <p class="endereco">{{ $contato->{trans('database.endereco')} }}</p>
            <p class="bairro-cidade">{{ $contato->{trans('database.bairro')} }} - {{ $contato->{trans('database.cidade')} }}</p>
            <p class="cep">{{ trans('frontend.footer.cep') }} {{ $contato->cep }}</p>
            <a href="{{ $contato->link_mapa }}" target="_blank" class="link-mapa">{{ trans('frontend.footer.mapa') }}</a>
            @php
            $telefone = str_replace("-", "", str_replace(" ", "", $contato->telefone));
            $celular = str_replace("-", "", str_replace(" ", "", $contato->celular));
            @endphp
            <a href="tel:{{ $telefone }}" class="telefone">T. {{ $contato->telefone }}</a>
            <a href="https://api.whatsapp.com/send?phone={{ $celular }}" class="celular" target="_blank">{{ trans('frontend.footer.celular') }} {{ $contato->celular }}</a>
            <a href="mailto:{{ $contato->email }}" target="_blank" class="email">{{ $contato->email }}</a>
            <div class="design">
                Site <a href="https://www.ditadesign.com.br/" class="link-design" target="_blank">dit.a design</a>
            </div>
        </div>
    </div>

</footer>