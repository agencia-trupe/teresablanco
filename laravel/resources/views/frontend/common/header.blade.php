<header>
    <div class="centralizado">
        <div class="idiomas">
            @foreach([
            'pt' => 'PORTUGUÊS',
            'en' => 'ENGLISH',
            'es' => 'ESPAÑOL',
            ] as $key => $title)
            <a href="{{ route('lang', $key) }}" class="lang">{{ $title }}</a>
            @endforeach
        </div>
        <a href="{{ route('home') }}" class="logo-header" title="{{ $config->title }}">
            <img src="{{ asset('assets/img/layout/marca-teresablanco.svg') }}" alt="" class="img-logo">
        </a>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines internas"></span>
        </button>
        <nav>
            @include('frontend.common.nav')
        </nav>
    </div>

</header>