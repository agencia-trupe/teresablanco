<div class="itens-menu">
    <a href="{{ route('home') }}" @if(Tools::routeIs('home*')) class="link-nav active" @endif class="link-nav"><span class="link-hover"></span>Home</a>
    <span class="ponto">.</span>
    <a href="{{ route('produtos.categorias', $categoria1->slug) }}" @if(Tools::routeIs('produtos*')) class="link-nav active" @endif class="link-nav"><span class="link-hover"></span>{{ trans('frontend.header.produtos') }}</a>
    <span class="ponto">.</span>
    <a href="" class="link-nav link-artista"><span class="link-hover"></span>{{ trans('frontend.header.artista') }}</a>
    <span class="ponto">.</span>
    <a href="" class="link-nav link-design"><span class="link-hover"></span>{{ trans('frontend.header.design') }}</a>
    <span class="ponto">.</span>
    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="link-nav active" @endif class="link-nav"><span class="link-hover"></span>{{ trans('frontend.header.contato') }}</a>
    <div class="redes-sociais">
        <a href="{{ $contato->facebook }}" target="_blank" class="facebook">
            <img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt="" class="img-rede">
        </a>
        <a href="{{ $contato->instagram }}" target="_blank" class="instagram">
            <img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="" class="img-rede">
        </a>
        @php $whatsapp = str_replace("-", "", str_replace(" ", "", $contato->celular)); @endphp
        <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="whatsapp" target="_blank">
            <img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt="" class="img-rede">
        </a>
    </div>
</div>