<footer class="footer-contato">

    <div class="direitos">
        <p class="dados-direitos">© {{ date('Y') }} {{ $config->title }} | {{ trans('frontend.footer.direitos') }}.</p>
    </div>
    <div class="design">
        Site <a href="https://www.ditadesign.com.br/" class="link-design" target="_blank">dit.a design</a>
    </div>

</footer>