@extends('frontend.common.template')

@section('content')

<section class="produtos-categorias">
    <div class="centralizado">
        <div class="submenu">
            @foreach($categorias as $cat)
            @if($cat->slug == $categoria->slug)
            <a href="{{ route('produtos.categorias', $cat->slug) }}" class="categoria active {{ $cat->slug }}">
                @if($cat->slug == 'poltronas')
                <div class="img-poltrona"></div>
                @elseif($cat->slug == 'bancos' || $cat->slug == 'banquetas')
                <div class="img-banco"></div>
                @else
                <div class="img-decoracao"></div>
                @endif
                {{ $cat->{trans('database.titulo')} }}
            </a>
            @else
            <a href="{{ route('produtos.categorias', $cat->slug) }}" class="categoria {{ $cat->slug }}">
                @if($cat->slug == 'poltronas')
                <div class="img-poltrona"></div>
                @elseif($cat->slug == 'bancos' || $cat->slug == 'banquetas')
                <div class="img-banco"></div>
                @else
                <div class="img-decoracao"></div>
                @endif
                {{ $cat->{trans('database.titulo')} }}
            </a>
            @endif
            @endforeach
        </div>

        <div class="produtos-categoria">
            <p class="caminho">HOME <span>></span> {{ trans('frontend.produtos.produtos') }} <span>></span> <strong>{{ $categoria->{trans('database.titulo')} }}</strong></p>
            <div class="categoria {{$categoria->slug}}">
                <img src="{{ asset('assets/img/categorias/'.$categoria->capa) }}" alt="" class="img-categoria">
                <div class="frase-categoria">{{ $categoria->{trans('database.frase')} }}</div>
                <img src="{{ asset('assets/img/layout/faixa-colorida-internas.svg') }}" alt="" class="img-faixa-colorida">
            </div>
            <div class="produtos">
                @foreach($produtos as $produto)
                <a href="{{ route('produtos.detalhes', [$categoria->slug, $produto->slug]) }}" class="produto" data-produto-id="{{ $produto->id }}">
                    @foreach($imagens as $imagem)
                    @if($imagem->produto_id == $produto->id)
                    <img src="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}" alt="" class="img-produto">
                    @break
                    @endif
                    @endforeach
                    <div class="overlay">
                        <span>{{ $produto->{trans('database.titulo')} }}</span>
                    </div>
                </a>
                @endforeach
            </div>

            <a href="" class="link-topo {{$categoria->slug}}" title="Voltar ao Topo">
                <p class="seta-topo">^</p>
                <p class="texto-topo">{{ trans('frontend.produtos.topo') }}</p>
            </a>
        </div>
    </div>
</section>

@endsection