@extends('frontend.common.template')

@section('content')

<section class="produtos-detalhes">
    <div class="centralizado">
        <div class="submenu">
            @foreach($categorias as $cat)
            @if($cat->slug == $categoria->slug)
            <a href="{{ route('produtos.categorias', $cat->slug) }}" class="categoria active {{ $cat->slug }}">
                @if($cat->slug == 'poltronas')
                <div class="img-poltrona"></div>
                @elseif($cat->slug == 'bancos' || $cat->slug == 'banquetas')
                <div class="img-banco"></div>
                @else
                <div class="img-decoracao"></div>
                @endif
                {{ $cat->{trans('database.titulo')} }}
            </a>
            @else
            <a href="{{ route('produtos.categorias', $cat->slug) }}" class="categoria {{ $cat->slug }}">
                @if($cat->slug == 'poltronas')
                <div class="img-poltrona"></div>
                @elseif($cat->slug == 'bancos' || $cat->slug == 'banquetas')
                <div class="img-banco"></div>
                @else
                <div class="img-decoracao"></div>
                @endif
                {{ $cat->{trans('database.titulo')} }}
            </a>
            @endif
            @endforeach
        </div>

        <div class="produto-dados">
            <p class="caminho">HOME <span>></span> {{ trans('frontend.produtos.produtos') }} <span>></span> {{ $categoria->{trans('database.titulo')} }} <span>></span> <strong>{{ $produto->{trans('database.titulo')} }}</strong></p>
            <div class="produto">
                <div class="produto-imagens">
                    <a href="{{ asset('assets/img/produtos/imagens/'.$capa->imagem) }}" class="capa-produto" data-produto-id="{{ $produto->id }}" imagem-id="{{ $capa->id }}">
                        <img src="{{ asset('assets/img/produtos/imagens/'.$capa->imagem) }}" alt="" class="img-capa">
                    </a>
                    <div class="imagens">
                        @foreach($imagens as $variacao)
                        <a href="{{ asset('assets/img/produtos/imagens/'.$variacao->imagem) }}" class="variacao" imagem="{{$variacao->imagem}}" imagem-id="{{ $variacao->id }}">
                            <img src="{{ asset('assets/img/produtos/imagens/'.$variacao->imagem) }}" alt="" class="img-variacao">
                        </a>
                        @endforeach
                    </div>
                </div>
                <div class="produto-infos">
                    <h4 class="titulo">{{ $produto->{trans('database.titulo')} }}</h4>
                    <div class="descricao">{!! $produto->{trans('database.descricao')} !!}</div>

                    @if($produto->{trans('database.peso')}) <p class="peso">{{ trans('frontend.produtos.peso') }}: <span>{{ $produto->{trans('database.peso')} }}</span></p> @endif
                    @if($produto->{trans('database.diametro')}) <p class="diametro">{{ trans('frontend.produtos.diametro') }}: <span>{{ $produto->{trans('database.diametro')} }}</span></p> @endif
                    @if($produto->{trans('database.altura')}) <p class="altura">{{ trans('frontend.produtos.altura') }}: <span>{{ $produto->{trans('database.altura')} }}</span></p> @endif
                    @if($produto->{trans('database.largura')}) <p class="largura">{{ trans('frontend.produtos.largura') }}: <span>{{ $produto->{trans('database.largura')} }}</span></p> @endif
                    @if($produto->{trans('database.profundidade')}) <p class="profundidade">{{ trans('frontend.produtos.profundidade') }}: <span>{{ $produto->{trans('database.profundidade')} }}</span></p> @endif

                    @if($produto->desenho)
                    <img src="{{ asset('assets/img/produtos/'.$produto->desenho) }}" alt="" class="img-desenho">
                    @endif

                    <button type="button" class="btn-mais-informacoes" id="btnMaisInformacoes" data-toggle="modal" data-target="#modalMaisInformacoes">{{ trans('frontend.produtos.mais-infos') }}</button>

                    @if($errors->any())
                    <div class="flash flash-erro">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                    @endif

                    @if(session('enviado'))
                    <div class="flash flash-sucesso">
                        <p>{{ trans('frontend.contato.sucesso') }}!</p>
                    </div>
                    @endif

                    <p class="frase1">{{ trans('frontend.produtos.exclusivas') }}.</p>
                    <P class="frase2">{{ trans('frontend.produtos.confira') }}.</P>

                    <a href="{{ route('contato') }}" class="btn-fale-conosco">
                        <p class="texto-btn">{{ trans('frontend.produtos.fale') }}</p>
                        <img src="{{ asset('assets/img/layout/ico-whatsapp-amarelo.svg') }}" alt="" class="img-wpp">
                    </a>

                    <div class="btns-ant-prox">
                        @if(isset($anterior))
                        <a href="{{ route('produtos.detalhes', [$categoria->slug, $anterior->slug]) }}" class="btn-anterior">« {{ trans('frontend.produtos.anterior') }}</a>
                        @endif
                        @if(isset($proximo))
                        <a href="{{ route('produtos.detalhes', [$categoria->slug, $proximo->slug]) }}" class="btn-proximo">{{ trans('frontend.produtos.proximo') }} »</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="produto-imgs-show" style="display:none;">
        @foreach($imagens as $imagem)
        <a href="{{ asset('assets/img/produtos/imagens/originais/'.$imagem->imagem) }}" class="fancybox {{ $imagem->id }}" rel="produto-{{ $produto->id }}" imagem-id="{{ $imagem->id }}"></a>
        @endforeach
    </div>
</section>

<!-- MODAL MAIS INFORMAÇÕES -->
<div class="modal fade" id="modalMaisInformacoes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
    <div class="modal-dialog modal-dialog-cadastro" role="document">
        <div class="modal-content">
            <h4 class="modal-titulo" id="myModalLabel">{{ trans('frontend.produtos.mais-infos') }}</h4>
            <form action="{{ route('mais-informacoes', $produto->id) }}" method="POST">
                {!! csrf_field() !!}
                <input type="text" name="nome" value="{{ old('nome') }}" placeholder="{{ trans('frontend.contato.nome') }}" required>
                <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="{{ trans('frontend.contato.telefone') }}">
                <textarea name="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-enviar">{{ trans('frontend.contato.enviar') }}</button>
            </form>
        </div>
    </div>
</div>

@endsection