<!doctype html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} - Painel Administrativo</title>

    <link rel="stylesheet" href="{{ asset('assets/vendor/bootswatch-dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/painel.css') }}">

</head>

<body class="painel-login">
    <div class="col-md-4 col-sm-6 col-xs-10 login">
        <h3>
            {{ config('app.name') }}
            <small>Painel Administrativo</small>
        </h3>

        <form class="form-horizontal" role="form" method="POST" action="{{ route('esqueci-minha-senha.post') }}" style="width: 100%;">
            {{ csrf_field() }}

            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-user"></i>
                    </span>
                    <input type="text" name="email" placeholder="e-mail" class="form-control" required>
                </div>
            </div>

            <div class="col-12" style="display:flex; flex-direction:column;">
                <button type="submit" class="btn btn-success col-6" style="margin-top:20px;">SOLICITAR REDEFINIÇÃO DE SENHA</button>
                <a href="{{ route('login') }}" class="btn btn-default col-6" style="margin-top:10px;">Voltar</a>
            </div>

            @if($errors->any())
            <div class="flash flash-erro" style="width:100%; height:50px; display:flex; align-items:center; justify-content:center; margin-top:20px; color:#FFF; background-color:#E02227;">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso" style="width:100%; height:60px; display:flex; align-items:center; justify-content:center; margin-top:20px; color:#FFF; background-color:#2caf2c;">
                <p style="margin:0; padding:0 10px;">Um e-mail foi enviado com instruções para a redefinição de senha.</p>
            </div>
            @endif

        </form>
    </div>
</body>

</html>