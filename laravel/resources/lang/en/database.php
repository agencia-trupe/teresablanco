<?php

return [
    'titulo'        => 'titulo_en',
    'frase'         => 'frase_en',
    'texto'         => 'texto_en',
    'descricao'     => 'descricao_en',
    'endereco'      => 'endereco_en',
    'bairro'        => 'bairro_en',
    'cidade'        => 'cidade_en',
    'frase_artista' => 'frase_artista_en',
    'texto_artista' => 'texto_artista_en',
    'frase_design'  => 'frase_design_en',
    'texto_design'  => 'texto_design_en',
    'peso'          => 'peso_en',
    'altura'        => 'altura_en',
    'largura'       => 'largura_en',
    'profundidade'  => 'profundidade_en',
    'diametro'      => 'diametro_en',
];
