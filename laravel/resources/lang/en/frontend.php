<?php

return [

    'header' => [
        'produtos' => 'Products',
        'artista'  => 'Artist',
        'design'   => 'Design',
        'contato'  => 'Contact',
    ],

    'footer' => [
        'whatsapp' => 'Talk to us on WhatsApp',
        'siga'     => 'Follow',
        'cep'      => 'ZIP CODE',
        'mapa'     => 'SEE MAP',
        'celular'  => 'Mobile',
        'direitos' => 'All rights reserved',
    ],

    'home' => [
        'artista' => 'THE ARTIST',
        'conheca' => 'LEARN MORE',
        'design'  => 'DESIGN',
        'siga'    => "Follow Teresa Blanco on Instagram to see what's new",
    ],

    'produtos' => [
        'produtos'     => 'PRODUCTS',
        'topo'         => 'TOP',
        'peso'         => 'Weight',
        'diametro'     => 'Diameter',
        'altura'       => 'Height',
        'largura'      => 'Width',
        'profundidade' => 'Depth',
        'mais-infos'   => 'MORE DETAILS PLEASE',
        'exclusivas'   => 'OUR PIECES ARE EXCLUSIVE',
        'confira'      => 'SEE WHAT WE HAVE IN STOCK AND OTHER MODELS TOO',
        'fale'         => 'CONTACT US',
        'anterior'     => 'previous',
        'proximo'      => 'next',
    ],

    'contato' => [
        'nome'     => 'name',
        'telefone' => 'telephone',
        'mensagem' => 'message',
        'enviar'   => 'SEND',
        'sucesso'  => 'Your message has been sent successfully',
        'checkbox' => "I'M AN ARCHITECT / INTERIOR DESIGNER",
        'recaptcha' => 'Invalid recaptcha',
    ],

    'cookies1' => 'We use cookies to personalize content, track ads and deliver a more secure browsing experience. By continuing to browse our site, you are agreeing to our use of this information. Read our',
    'politica' => 'Privacy Policy',
    'cookies2' => 'for more details.',
    'aceitar'  => 'ACCEPT AND CLOSE',
];
