<?php

return [
    'titulo'        => 'titulo_es',
    'frase'         => 'frase_es',
    'texto'         => 'texto_es',
    'descricao'     => 'descricao_es',
    'endereco'      => 'endereco_es',
    'bairro'        => 'bairro_es',
    'cidade'        => 'cidade_es',
    'frase_artista' => 'frase_artista_es',
    'texto_artista' => 'texto_artista_es',
    'frase_design'  => 'frase_design_es',
    'texto_design'  => 'texto_design_es',
    'peso'          => 'peso_es',
    'altura'        => 'altura_es',
    'largura'       => 'largura_es',
    'profundidade'  => 'profundidade_es',
    'diametro'      => 'diametro_es',
];
