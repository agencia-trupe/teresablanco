<?php

return [

    'header' => [
        'produtos' => 'Productos',
        'artista'  => 'Artista',
        'design'   => 'Design',
        'contato'  => 'Contacto',
    ],

    'footer' => [
        'whatsapp' => 'Contáctenos por WhatsApp',
        'siga'     => 'Siga',
        'cep'      => 'CODIGO POSTAL',
        'mapa'     => 'VER EL MAPA',
        'celular'  => 'C.',
        'direitos' => 'Todos los derechos reservados',
    ],

    'home' => [
        'artista' => 'ARTISTA',
        'conheca' => 'CONOZCA',
        'design'  => 'DESIGN',
        'siga'    => 'Siga a Teresa Blanco en Instagram y vea las novedades',
    ],

    'produtos' => [
        'produtos'     => 'PRODUCTOS',
        'topo'         => 'TOPE',
        'peso'         => 'Peso',
        'diametro'     => 'Diámetro',
        'altura'       => 'Altura',
        'largura'      => 'Ancho',
        'profundidade' => 'Profundidad',
        'mais-infos'   => 'QUIERO MÁS INFORMACIONES',
        'exclusivas'   => 'NUESTRAS PIEZAS SON EXCLUSIVAS',
        'confira'      => 'VEA LA DISPONIBILIDAD Y OTROS MODELOS',
        'fale'         => 'CONTÁCTENOS',
        'anterior'     => 'anterior',
        'proximo'      => 'seguiente',
    ],

    'contato' => [
        'nome'     => 'nombre',
        'telefone' => 'telefóno',
        'mensagem' => 'mensaje',
        'enviar'   => 'ENVIAR',
        'sucesso'  => 'Mensaje enviado con éxito',
        'checkbox' => 'SOY ARQUITECTO / DISEÑADOR DE INTERIORES',
        'recaptcha' => 'Recaptcha inválido',
    ],

    'cookies1' => 'Usamos cookies para personalizar el contenido, monitorear anuncios y ofrecerle una experiencia de navegación más segura a usted. Al continuar navegando en nuestro sitio web usted concuerda con el uso de estas informaciones. Lea nuestra',
    'politica' => 'Política de Privacidad',
    'cookies2' => 'y conozca más.',
    'aceitar'  => 'ACEPTAR Y CERRAR',
];
