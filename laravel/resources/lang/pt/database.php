<?php

return [
    'titulo'        => 'titulo_pt',
    'frase'         => 'frase_pt',
    'texto'         => 'texto_pt',
    'descricao'     => 'descricao_pt',
    'endereco'      => 'endereco_pt',
    'bairro'        => 'bairro_pt',
    'cidade'        => 'cidade_pt',
    'frase_artista' => 'frase_artista_pt',
    'texto_artista' => 'texto_artista_pt',
    'frase_design'  => 'frase_design_pt',
    'texto_design'  => 'texto_design_pt',
    'peso'          => 'peso_pt',
    'altura'        => 'altura_pt',
    'largura'       => 'largura_pt',
    'profundidade'  => 'profundidade_pt',
    'diametro'      => 'diametro_pt',
];
