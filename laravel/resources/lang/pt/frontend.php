<?php

return [

    'header' => [
        'produtos' => 'Produtos',
        'artista'  => 'Artista',
        'design'   => 'Design',
        'contato'  => 'Contato',
    ],

    'footer' => [
        'whatsapp' => 'Fale com a gente pelo WhatsApp',
        'siga'     => 'Siga',
        'cep'      => 'CEP',
        'mapa'     => 'VER O MAPA',
        'celular'  => 'C.',
        'direitos' => 'Todos os direitos reservados',
    ],

    'home' => [
        'artista' => 'ARTISTA',
        'conheca' => 'CONHEÇA',
        'design'  => 'DESIGN',
        'siga'    => 'Siga Teresa Blanco no instagram e acompanhe as novidades',
    ],

    'produtos' => [
        'produtos'     => 'PRODUTOS',
        'topo'         => 'TOPO',
        'peso'         => 'Peso',
        'diametro'     => 'Diâmetro',
        'altura'       => 'Altura',
        'largura'      => 'Largura',
        'profundidade' => 'Profundidade',
        'mais-infos'   => 'QUERO MAIS INFORMAÇÕES',
        'exclusivas'   => 'NOSSAS PEÇAS SÃO EXCLUSIVAS',
        'confira'      => 'CONFIRA A DISPONIBILIDADE E OUTROS MODELOS',
        'fale'         => 'FALE COM A GENTE',
        'anterior'     => 'anterior',
        'proximo'      => 'próximo',
    ],

    'contato' => [
        'nome'     => 'nome',
        'telefone' => 'telefone',
        'mensagem' => 'mensagem',
        'enviar'   => 'ENVIAR',
        'sucesso'  => 'Mensagem enviada com sucesso',
        'checkbox' => 'SOU ARQUITETO / DESIGNER DE INTERIORES',
        'recaptcha' => 'Recaptcha inválido',
    ],

    'cookies1' => 'Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa',
    'politica' => 'Política de Privacidade',
    'cookies2' => 'e saiba mais.',
    'aceitar'  => 'ACEITAR E FECHAR',
];
