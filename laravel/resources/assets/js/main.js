import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(document).ready(function () {
  var urlPrevia = "";
  // var urlPrevia = "/previa-teresablanco";

  // HEADER ARTISTA - âncora home/artista
  $("header .link-nav.link-artista").click(function (e) {
    e.preventDefault();
    var element = "#artista";

    if (window.location.href == routeHome) {
      var targetOffset = $("#homeArtista").offset().top;
      $("html, body").animate({ scrollTop: targetOffset - 200 }, 800);
    } else {
      window.location.href = routeHome + element;
    }
  });

  // HEADER DESIGN - âncora home/design
  $("header .link-nav.link-design").click(function (e) {
    e.preventDefault();
    var element = "#design";

    if (window.location.href == routeHome) {
      var targetOffset = $("#homeDesign").offset().top;
      $("html, body").animate({ scrollTop: targetOffset - 100 }, 1200);
    } else {
      window.location.href = routeHome + element;
    }
  });

  // âncoras vindo de outras páginas
  $(window).on("load", function () {
    var hash = window.location.hash;
    if (hash) {
      if (hash == "#artista") {
        var targetOffset = $("#homeArtista").offset().top;
        $("html, body").animate({ scrollTop: targetOffset - 200 }, 800);
      }
      if (hash == "#design") {
        var targetOffset = $("#homeDesign").offset().top;
        $("html, body").animate({ scrollTop: targetOffset - 100 }, 1200);
      }
    }
  });

  // HOME - BANNERS
  $(".banners").cycle({
    slides: ".banner",
    fx: "fade",
    speed: 800,
    pager: ".cycle-pager",
  });

  // HOME - parallax artista (scroll)
  $(window).on("load scroll", function () {
    var parallaxElement = $(".parallax_scroll"),
      parallaxQuantity = parallaxElement.length;
    window.requestAnimationFrame(function () {
      for (var i = 0; i < parallaxQuantity; i++) {
        var currentElement = parallaxElement.eq(i),
          windowTop = $(window).scrollTop(),
          elementTop = currentElement.offset().top,
          elementHeight = currentElement.height(),
          viewPortHeight = window.innerHeight * 0.5 - elementHeight * 0.5,
          scrolled = windowTop - elementTop + viewPortHeight;
        currentElement.css({
          transform: "translate3d(0," + scrolled * -0.15 + "px, 0)",
        });
      }
    });
  });

  // PRODUTOS - removendo margin 3º produto
  $(
    ".produtos-categoria .produtos .produto:nth-child(3n), .produtos-detalhes .produto-imagens .variacao:nth-child(4n)"
  ).css("margin-right", "0");

  // PRODUTOS - btn topo
  $(".link-topo").click(function (e) {
    e.preventDefault();
    $("html, body").animate({ scrollTop: $("body").offset().top - 200 }, 1000);
  });

  // PRODUTO - variações
  $(".produtos-detalhes .produto-imagens .variacao").click(function (e) {
    e.preventDefault();
    $(".produtos-detalhes .produto-imagens .variacao.active").removeClass(
      "active"
    );
    $(this).addClass("active");
    var src = $(this).attr("href");
    $(".produtos-detalhes .produto-imagens .capa-produto .img-capa").attr(
      "src",
      src
    );
    var href =
      window.location.href +
      urlPrevia +
      "/assets/img/produtos/imagens/originais/" +
      $(this).attr("imagem");
    $(".produtos-detalhes .produto-imagens .capa-produto").attr("href", href);
    $(".produtos-detalhes .produto-imagens .capa-produto").attr(
      "imagem-id",
      $(this).attr("imagem-id")
    );
  });

  // PRODUTO - fancybox imagens
  $(".fancybox").fancybox({
    padding: 0,
    prevEffect: "fade",
    nextEffect: "fade",
    closeBtn: false,
    openEffect: "elastic",
    openSpeed: "150",
    closeEffect: "elastic",
    closeSpeed: "150",
    helpers: {
      title: {
        type: "outside",
        position: "top",
      },
      overlay: {
        css: {
          background: "rgba(132, 134, 136, .88)",
        },
      },
    },
    fitToView: false,
    autoSize: false,
    beforeShow: function () {
      this.maxWidth = "90%";
      this.maxHeight = "90%";
    },
  });

  $(".produtos-detalhes .produto-imagens .capa-produto").click(function handler(
    e
  ) {
    e.preventDefault();
    const id = $(this).data("produto-id");
    var imagemId = $(this).attr("imagem-id");
    if (id) {
      $(`a[rel=produto-${id}]` + "." + imagemId).click();
    }
  });

  // MODAL - mais informacoes
  $("#btnMaisInformacoes").click(function () {
    $("#modalMaisInformacoes").css("display", "flex");
  });
  $(".btn-close").click(function () {
    $("#modalMaisInformacoes").css("display", "none");
  });

  // AVISO DE COOKIES
  $(".aviso-cookies").show();

  $(".aceitar-cookies").click(function () {
    var url = window.location.origin + "/aceite-de-cookies";

    $.ajax({
      type: "POST",
      url: url,
      success: function (data, textStatus, jqXHR) {
        $(".aviso-cookies").hide();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  });
});
